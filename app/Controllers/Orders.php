<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\OrdersModel;

class Orders extends Controller
{

    public function index()
    {
        $model = new OrdersModel();
        $data['orders']  = $model->getOrders()->getResult();
        // var_dump($data);
        echo view('orders_view', $data);
    }

    // show order by id
    public function show_order($id){  
        $model = new OrdersModel();   
        
        $data['order'] =  $model->join('customers', 'customer = id','left');
        $data['order'] =  $model->join('products', 'product = id_product','left');
        $data['order'] =  $model->where('id_order', $id)->first();
        // var_dump($data);
        return view('view-orders', $data);
      
    }
  
}