<?php

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\ReviewsModel;
 
class Reviews extends Controller
{
    private $review = '' ;
    public function __construct(){
      
        $this->review = new ReviewsModel();       
    }
    // Index() – This is used to display .
    // create() – This method is used to display create form.
    // store() – This is method is used to insert into the MySQL database.
    // update() – This is used to validate the form data server-side and update it into the MySQL database.
    // edit() – This method is used to display a single user.
    // delete() – This method is used to delete data from MySQL database.
    public function index()
    {
       
        $session = session(); 
        $this->review->join('customers', 'review__customer = id','INNER');  
        $this->review->join('products', 'review__product = id_product','INNER');  
        $data['reviews'] = $this->review->orderBy('id', 'DESC')->findAll();       
           
        return view('reviews',$data);
    }
   
    
 
}
 
  
   