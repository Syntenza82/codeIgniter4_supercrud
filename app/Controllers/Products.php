<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ProductModel;

class Products extends Controller
{
    private $product = '' ;
    public function __construct(){
      
        $this->product = new ProductModel();       
    }
    
    // show product list
    public function index()
    {
        $session = session(); 
        $this->product->join('product_categories', 'category_id = category','left');
        $data['products'] = $this->product->orderBy('id_product', 'DESC')->findAll();   
        $data['categories'] = $this->product->getCategory()->getResult();    
        return view('products',$data);
    }
    
    // insert data
    public function store() {
       
        $data = [
            'image' => $this->request->getVar('image'),
            'name' => $this->request->getVar('name'),
            'description'  => $this->request->getVar('description'),
            'price'  => $this->request->getVar('price'),
            'category'  => $this->request->getVar('category'),          
            'create_date'  => $this->request->getVar('create_date'),
        ];
        
        $this->product->insert($data);    
        $session = session(); 
        $session->setFlashdata('msg', 'Product Successfully Added');  
        return redirect()->to( base_url('public/index.php/products') ); 
        // return $this->response->redirect(site_url('/products'));
    }

    // show product by id
    public function edit($id){
        $this->product->join('product_categories', 'category_id = category','left');
        $data['product'] = $this->product->where('id_product', $id)->first();
        $data['categories'] = $this->product->getCategory()->getResult();  
        return view('edit-products', $data);
    }

    // update product data
    public function update(){

        helper(['form', 'url']);

        
        $id = $this->request->getVar('id_product');
        $data = [
            'image' => $this->request->getVar('image'),
            'name' => $this->request->getVar('name'),
            'description'  => $this->request->getVar('description'),
            'price'  => $this->request->getVar('price'),
            'category'  => $this->request->getVar('product_category'),          
            'create_date'  => $this->request->getVar('create_date'),
        ];
        $this->product->update($id, $data); 
        $session = session(); 
        $session->setFlashdata('msg', 'Product Successfully Updated');  
        return redirect()->to( base_url('public/index.php/products') );       
        // return $this->response->redirect(site_url('/products'));
    }

     // delete product
     public function delete($id = null){
      
        $session = session(); 
        $session->setFlashdata('msg', 'Product Successfully Deleted');  
        $data['products'] =$this->product->where('id_product', $id)->delete();

     return redirect()->to( base_url('public/index.php/products') );
    }   
}