<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\LoginModel;

class Validation  extends CI_Controller {

    public function __construct(){
        parent::__construct();
         $this->load->library('form_validation'); 
    }
    public function index(){
             
        $this->load->view('form_validation');
    }       
    public function form_validate(){
      
         
            $this->form_validation->set_rules('last_name', 'Name ', 'required');
            $this->form_validation->set_rules('image', 'Image ', 'required');
            $this->form_validation->set_rules('description','Description', 'required');
            $this->form_validation->set_rules('price','Price', 'required');
            $this->form_validation->set_rules('category','Category', 'required');
            $this->form_validation->set_rules('create_date','Create_date', 'required');
            $this->form_validation->set_rules('email', 'Email ', 'required');
            $this->form_validation->set_rules('password', 'Password ', 'required');
            if ($this->form_validation->run() == FALSE){
                       
                $this->load->view('form_validation');
            }else{
                
                echo "Form is submitted";
                echo "<pre>";
                print_r($this->input->post());
            }          
         
    }    
}