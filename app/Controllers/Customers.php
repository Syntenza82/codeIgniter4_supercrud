<?php

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\CustomersModel;
 
class Customers extends Controller
{
    
    // Index() – This is used to display .
    public function index()
    {
        $session = session();

        $model = new CustomersModel();
 
        $data['customers'] = $model->orderBy('id', 'DESC')->findAll();

        return view('customers', $data);
    }

  // create() – This method is used to display create form.
    public function create()
    {
        return view('create-customers');
    }

 /**
  *   store() – This is method is used to insert into the MySQL database.
  */
    public function store()
    {
 
        helper(['form', 'url']);

        $model = new CustomersModel();
 
        $data = [
 
            'avatar' => $this->request->getVar('avatar'),
            'first_name' => $this->request->getVar('first_name'),
            'last_name' => $this->request->getVar('last_name'),
            'password' => $this->request->getVar('password'),
            'email'  => $this->request->getVar('email'),
            ];
            $session = session(); 
        $session->setFlashdata('msg', 'Customer Successfully Insert');
        var_dump($_SESSION['msg']);
        $save = $model->insert($data);
        
         return redirect()->to( base_url('public/index.php/customers') );
    }

  // edit() – This method is used to display a single user.
    public function edit($id = null)
    {

     $model = new CustomersModel();
 
     $data['customer'] = $model->where('id', $id)->first();
     return view('edit-customers', $data);
    }

 /**
  * update() – This is used to validate the form data server-side and update it into the MySQL database.
  */
    public function update()
    {
 
        helper(['form', 'url']);

        $model = new CustomersModel();
 
        $id = $this->request->getVar('id');
 
        $data = [
 
            'first_name' => $this->request->getVar('first_name'),
            'last_name' => $this->request->getVar('last_name'),
            'email'  => $this->request->getVar('email'),
            ];
 
        $save = $model->update($id,$data);
        $session = session(); 
        $session->setFlashdata('msg', 'Customer Successfully Updated');  
        return redirect()->to( base_url('public/index.php/customers') );
    }

 /**
  *delete() – This method is used to delete data from MySQL database.
  */
    public function delete($id = null)
    {
 
     $model = new CustomersModel();
     $session = session(); 
     $session->setFlashdata('msg', 'Customer Successfully Deleted');     
     $data['customers'] = $model->where('id', $id)->delete();

     return redirect()->to( base_url('public/index.php/customers') );
    }

/**
 * Validation new customer
 */
    public function myForm()
    {
        if ($this->request->getMethod() == "post") {

            $file = $this->request->getFile("avatar");

            $file_type = $file->getClientMimeType();

            $valid_file_types = array("image/png", "image/jpeg", "image/jpg");

            $session = session();

            if (in_array($file_type, $valid_file_types)) {

                $profile_image = $file->getName();
              
              	// We can also use it like this. Automatically move function place the default image name into specified location.
                // Second value will be more beneficiary when we want to give a different name to the uploaded file.
                // $file->move("images"); 

                if ($file->move("images", $profile_image)) {

                    $userModel = new UserModel();

                    $data = [
                        "name" => $this->request->getVar("name"),
                        "email" => $this->request->getVar("email"),
                        "phone_no" => $this->request->getVar("phone_no"),
                        "profile_image" => "/images/" . $profile_image,
                    ];

                    if($userModel->insert($data)){

                        $session->setFlashdata("success", "Data saved successfully");
                    }else{

                        $session->setFlashdata("error", "Failed to save data");
                    }
                } else {
                    $session->setFlashdata("error", "Failed to move file");
                }
            } else {
                // invalid file type
                $session->setFlashdata("error", "Invalid file type selected");
            }

            return redirect()->to(base_url());

        }

        return view("customers");
    }
}



