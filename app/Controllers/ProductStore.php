<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ProductStoreModel;

class ProductStore extends Controller

{
    private $product = '' ;

    public function __construct(){
      
        $this->product = new ProductStoreModel();       
    }


	public function index()
	{	
	    $session = session(); 
        $session->setFlashdata('msg', 'Products loaded'); 
        $this->product->join('product_categories', 'category_id = category','left');
        $data['products'] = $this->product->orderBy('id_product', 'DESC')->findAll();   
        $data['categories'] = $this->product->getCategory()->getResult();  
        $session->setFlashdata('categories',  $data['categories']); 
        // var_dump($data);
        return view('product_store',$data); 
	}

    public function view($category = null)
    {
     $data['products'] =  $this->product->where('category', $category)->findAll();
    //  var_dump($data);
     return view('product_store', $data);
    }

}
