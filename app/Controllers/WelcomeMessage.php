<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\WelcomeMessageModel;

class WelcomeMessage extends Controller
{
    
	private $product = '' ;

    public function __construct(){
      
        $this->product = new WelcomeMessageModel();       
    }


	public function index()
	{	
        $session = session();
        $session->get('name');
        $session->get('email');
	    //  $this->product->join('products', 'category = category_id ','left');
        $data['categories'] = $this->product->orderBy('category_name', 'ASC')->findAll();   
        // echo json_encode($data['products']);
        //   var_dump($data);
        return view('welcome_message',$data);
	}

}
