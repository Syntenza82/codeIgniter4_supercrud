<?php namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
 
class CustomersModel extends Model
{
    protected $table = 'customers';
 
    protected $allowedFields = ['avatar','first_name','last_name','password','email'];
}
