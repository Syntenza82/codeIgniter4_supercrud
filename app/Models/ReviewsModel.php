<?php 
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
 
class ReviewsModel extends Model
{
    protected $table = 'reviews';
 
    protected $allowedFields = ['title','body', 'rating','customer','product','review_date'];
}
