<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class ProductStoreModel extends Model
{
	protected $table                = 'products';
	protected $primaryKey           = 'id_product';
	protected $allowedFields        = ['category','name','image','description','price','create_date'];


    public function getCategory()
    {
        $builder = $this->db->table('product_categories');
        return $builder->get();
    }

}
