<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Database\ConnectionInterface;

class OrdersModel extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id_order';    
    protected $allowedFields = ['name', 'description','price','category','image','create_date'];
    
    public function getOrders()
    {
        $builder = $this->db->table('orders');
        $builder->select('*');
        $builder->join('customers', 'customer = id','left');
        $builder->join('products', 'product = id_product','left');
        $builder->join('product_categories', 'category = category_id','left');
        $builder->orderBy('order_date', 'DESC');
        // $builder->groupBy('customers.last_name');
        return $builder->get();
    }

  
}

