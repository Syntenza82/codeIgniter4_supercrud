<?php 
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class ProductModel extends Model
{
        protected $table = 'products';
        protected $primaryKey = 'id_product';    
        protected $allowedFields = ['name', 'description','price','image','create_date','category'];

        public function getCategory()
    {
        $builder = $this->db->table('product_categories');
        return $builder->get();
    }

}