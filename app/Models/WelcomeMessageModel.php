<?php

namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class WelcomeMessageModel extends Model
{
	protected $table                = 'product_categories';
	protected $primaryKey           = 'category_id';
	protected $allowedFields        = ['category_name', 'category_description','category_image','create_date','category','name','image','description','price','create_date'];


	// public function getProduct()
    // {
    //     $builder = $this->db->table('products');
    //     // $builder = $this->db->distinct('name');
    //     return $builder->get();
    // }

}
