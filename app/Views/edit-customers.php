<!DOCTYPE html>
<html>
<head>
<title>Edit User</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
</head>
<header>
  <?php include "nav.php";?>
</header>
<body>
    <div class="container">
    <br>
    <?= \Config\Services::validation()->listErrors(); ?>

    <span class="d-none alert alert-success mb-3" id="res_message"></span>
    <div class="row">
        <h1>Manage Customer</h1>
        <a class="btn btn warning" style="color:#ff2000" type="button" href="<?= base_url('public/index.php/customers');?>">Retour</a>
        <div class="col-md-12">
            <form action="<?php echo base_url('public/index.php/customers/update');?>" name="edit-customers" id="edit-customer" method="post" accept-charset="utf-8">
            <p class="text-center"><img style="width:25%;"  src="<?=base_url();?>/public/assets/img/avatar/<?= $customer['avatar'] ?>"/><p>
                <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $customer['id'] ?>">
                <div class="form-group">
                    <label for="first_name">FirstName</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Please enter your first-name" value="<?php echo $customer['first_name'] ?>">
                </div>
                <div class="form-group">
                    <label for="last_name">LastName</label>
                    <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Please enter your last-name" value="<?php echo $customer['last_name'] ?>">
                </div> 
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="Please enter email id" value="<?php echo $customer['email'] ?>">
                </div>   
                <div class="form-group">
                    <button type="submit" id="send_form" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
    </div>
<script>
if ($("#edit-customer").length > 0) {
$("#edit-customer").validate({
rules: {
first_name: {
required: true,
},
last_name: {
required: true,
},
email: {
required: true,
maxlength: 50,
email: true,
},   
},
/************************************************* */
messages: {
first_name: {
required: "Please enter First_name",
},
last_name: {
required: "Please enter Last_name",
},
email: {
required: "Please enter valid email",
email: "Please enter valid email",
maxlength: "The email name should less than or equal to 50 characters",
}, 
},
})
}
</script>
</body>
</html>