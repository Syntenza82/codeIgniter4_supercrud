<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Reviews</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
</head>
<body>
  <header>
    <?php include "nav.php";?>
  </header>
  <div class="container mt-5">
    <h1>Reviews</h1>
    
    <div class="row cardrow mt-3">
        <?php if($reviews): ?>
          <?php foreach($reviews as $review): ?>
            <?php $rating = $review['rating']/2; ?>
            <div class="card col-md-3 mb-4">
              <div class="body">
                <img style="width:30%;border-radius:50%" src="<?=base_url();?>/public/assets/img/avatar/<?= $review['avatar']?>"/>
                <img style="width:35%;float:right" src="<?=base_url();?>/public/assets/img/article/<?= $review['image']?>"/>
                <h4><b><?=$review['title']?></b></h4>
                <?php for($x=1;$x<=$rating;$x++) { ?>
                    <img width='10%' src='<?=base_url();?>/public/assets/img/site/star.png'>
                  <?php }  ?>
                  <?php  if(($rating) < 1){
                      echo '<img width="10%" src="http://localhost/crud_codeIgniter/public/assets/img/site/star-empty.png">';
                  }?>
                  <?php
                    $datetime  = $review['review_date'];
                    $date = explode(" ", $datetime);
                    $newDate = str_replace("-","/", $date[0]);
                    echo date("d/m/Y",strtotime($newDate));
                  ?>
                  <p><?=$review['body']?></p> 
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        
  </div>
  
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  
  <script>
      var data;
      var reviews;
    $(document).ready( function () {

      reviews = <?php echo json_encode($reviews);?>;
      data = <?php echo json_encode($date[0]);?>;

    /****************************REVERSE DATE****************************/
      function inverseDate(sDate) {
        var aOfDates = sDate.split("-");
        return aOfDates[2] + "/" + aOfDates[1] + "/" + aOfDates[0];
      }
        console.log(inverseDate(data));
      });
  </script>
</body>
</html>