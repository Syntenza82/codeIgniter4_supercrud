<!DOCTYPE html>
<html>
<head>
<title>Codeigniter 4 Edit Product Form With Validation Example</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
</head>
<body>
<div class="container">
    <h1 style="color:#fff" class="mb-1 mt-5">Manage products</h1>
    <a class="btn btn warning" style="color:#ff2000" type="button" href="<?= base_url('public/index.php/products');?>">Retour</a>
    <br>
    <?= \Config\Services::validation()->listErrors(); ?>
    <span class="d-none alert alert-success mb-3" id="res_message"></span>
    <div class="row">
        <div class="col-md-12" style="color:#fff">
<!--Form--><form action="<?php echo base_url('public/index.php/products/update');?>" name="edit-product" id="edit-product" method="post" accept-charset="utf-8">
                <p class="text-center"><img style="width:25%;"  src="<?=base_url();?>/public/assets/img/article/<?= $product['image'] ?>"/><p>
                <input type="hidden" name="id_product" class="form-control" id="id_product" value="<?php echo $product['id_product'] ?>">
                <div class="form-group">
                    <label for="image">Images</label>
                    <input type="text" name="image" class="form-control" id="image" placeholder="Please enter image" value="<?php echo $product['image'] ?>"required>
                </div>
                <div class="form-group">
                        <label for="category">Category</label>
                        <select name="product_category" id="product_category" class="form-control">
                            <option value="" selected><?= $product['category_name'];?></option>
                            <?php foreach($categories as $row):?>
                            <option value="<?= $row->category_id?>"><?= $row->category_name?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Please enter name" value="<?php echo $product['name'] ?>"required>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" name="description" class="form-control" id="description" placeholder="Please enter description" value="<?php echo $product['description'] ?>"  required/>
                </div> 
                <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" name ="price" id ="price" class="form-control"  placeholder="Please enter price" value="<?php echo $product['price'] ?>" required>
                    </div>
                <div class="form-group">
                    <label for="create_date">Create date</label>
                    <input type="text" name="create_date" class="form-control" id="create_date" placeholder="Please enter date" value="<?php echo $product['create_date'] ?>" required>
                </div>   
                <div class="form-group">
                   <p class="text-center"><button type="submit" id="send_form" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
    </div>
<script>
if ($("#edit-product").length > 0) {

        $("#edit-product").validate({

            rules: {
            image: {
                required: true,
            },
            name: {
                required: true,
            },
            description: {
                required: true,
                maxlength: 500,
            },  
            price: {
                required: true,
               
            }, 
            category: {
                required: true,
            },
            create_date: {
                required:true,
            }
        },
        messages: {

                image: {
                    required: "Please upload your image",
                },
                name: {
                    required: "Please enter name of product",
                },
                description: {
                    required: "Please enter description",
                    maxlength: "The description  should less than or equal to 500 characters",
                },
                price: {
                    required: "Please enter price",
                },
                category: {
                    required: "Please enter category",
                },
                create_date: {
                    required: "Please enter date",
                } ,
        },
    })
}
</script>
</body>
</html>