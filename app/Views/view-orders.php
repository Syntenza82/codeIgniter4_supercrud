<!DOCTYPE html>
<html>
<head>
<title>View Order</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
<link href="<?= base_url(); ?>/public/assets/css/view-orders.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
</head>

<body>
<a class="btn btn warning" style="color:#ff2000" type="button" href="<?= base_url('public/index.php/orders');?>">Retour</a>
<div class="container">
    <div class="card"> 
    <div class="card-body">
        <h3>Article : <?= $order['name'] ;?></h3>
        <h4>Date de la commande : <?=$order['order_date']?></h4>
        <p class="price">N° : <?=$order['order_number']?></p>
        <p class="price">Montant : <?=$order['price']?>.00 €</p>
        <h1 style="color:#000"> Nom Client : <span style="color:green"> <?= $order['first_name'] . ' ' . $order['last_name']; ?></span></h1>
        <img class="product-order" src="<?=base_url();?>/public/assets/img/article/<?= $order['image'] ?>">
        <img class="avatar" src="<?=base_url();?>/public/assets/img/avatar/<?= $order['avatar'] ?>">
        <p><?= $order['description'] ;?></p>
        </div>
        <a class="btn btn default" style="color:#ff2000" type="button" href="<?= base_url('public/index.php/reviews');?>">Reviews</a>
    </div>
</div>
</body>
</html>