<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
    <link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
        <?php include "nav.php";?>
    </header>
    <?php
        if(isset($_SESSION['msg'])){
            echo "<h1 class='msgSession'>" . $_SESSION['msg'] . "</h1>";
        }
    ?>
    <div class="container mt-3">
        <div class="table-responsive mt-2">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6 mb-5">
                            <h1>Manage<b> Products</b></h1>
                        </div>
                        <div class="col-sm-6">
                            <a href="#addProductModal" class="btn btn-success" data-toggle="modal"><i class="material-icons"></i> <span>Add Product</span></a>
                        </div>
                    </div>
                </div>
                
                <table class="table table-striped  table-bordered" id="products">
                    <thead>
                        <tr> 
                            <th>Image</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($products as $product) { ?>
                        <tr>
                            <td><img class="product-order" src="<?=base_url();?>/public/assets/img/article/<?= $product['image'] ?>"></td>
                            <td><?=$product['name']?></td>
                            <td>
                                <a href="<?php echo base_url('public/index.php/products/edit/'.$product['id_product']);?>" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit"></i></a>
                                <a href="<?php echo base_url('public/index.php/products/delete/'.$product['id_product']);?>" class="delete"><i class="material-icons" data-toggle="tooltip" title="Delete"></i></a>                      
                            </td>
                        </tr>
                    <?php } ?>  
                    </tbody>
                </table>
                
            </div>
        </div>        
    </div>

    <footer>			
        <?php include "footer.php";?>
    </footer>
    <!-- Add Modal HTML -->
    <div id="addProductModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method ="post" action="<?= base_url('public/index.php/products/store') ?>" id="addProduct">
                    <div class="modal-header">                        
                        <h4 class="modal-title">Add Product</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body"> 
                            <div class="form-group">
                                <label>Image</label>
                                <input type="text" name ="image" id ="image" class="form-control" >
                            </div>                   
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name ="name" id ="name" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea rows="6" type="text"  name ="description" id ="description" class="form-control" ></textarea>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="number" name ="price" id ="price" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="">-Select-</option>
                                    <?php foreach($categories as $row):?>
                                    <option value="<?= $row->category_id?>"><?= $row->category_name?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>  
                            <div class="form-group">
                                <label>Create_date</label>
                                <input type="text"  name ="create_date" name ="create_date" class="form-control" >
                            </div>                    
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default"value="Cancel">
                        <input type="submit" class="btn btn-info" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <!-- Delete Modal HTML -->
    <div id="deleteProductModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
            <form method ="post" action="<?= site_url('/delete-product') ?>">
                    <div class="modal-header">                        
                        <h4 class="modal-title">Delete Product</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">                    
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                        <input type="hidden" id="delete_id" name="delete_id">
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script>
    /***************************************************
    @author @LudoLabTeam - Ludovic Mouly
    * @copyright  1920-2080 DEVLM - CDA 20206
    ***************************************************/
    $(document).ready( function () {
        $('#products').DataTable();
    //       document.getElementById('addProductModal').addEventListener('click',function(){
    //     alert('tu as cliqué!!!!!!!!!!!!!!!!');
    // });

    /****************************VALIDATION FORM *********************************************/

    if ($("#addProduct").length > 0) {
        console.log($("#addProduct").length);
        $("#addProduct").validate({

            rules: {
                image: {
                    required: true,
                },
                name: {
                    required: true,
                },
                description: {
                    required: true,
                    maxlength: 500,
                },  
                price: {
                    required: true,
                
                }, 
                category: {
                    required: true,
                },
                create_date: {
                    required:true,
                }
            },
            messages: {

                    image: {
                        required: "Please upload your image",
                    },
                    name: {
                        required: "Please enter name of product",
                    },
                    description: {
                        required: "Please enter description",
                        maxlength: "The description  should less than or equal to 500 characters",
                    },
                    price: {
                        required: "Please enter price",
                    },
                    category: {
                        required: "Please enter category",
                    },
                    create_date: {
                        required: "Please enter date",
                    } ,
            },
        })
    }
        
    });
    </script>
</body>
</html>