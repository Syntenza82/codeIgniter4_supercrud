<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
    <title>Orders Lists</title>
    <!-- <link rel="stylesheet" href="/css/bootstrap.min.css"> -->
</head>
<body>
<header>
  <?php include "nav.php";?>
</header>
    <div class="container">

        <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6 mb-5">
                            <h1>Orders<b> List</b></h1>
                        </div>
                        <div class="col-sm-6">
                            <p class="text-center"><a href="<?php echo site_url('public/index.php/home') ?>"><img  style="width:7%;float:right" src="../assets/img/site/left.png"/></a></p>
                        </div>
                    </div>
        </div>
        <table class="table table-striped table-bordered" id="orders">
            <thead>
                <tr>
                    <th>Date Commande</th>
                    <th>Nom</th>
                    <th>Image</th>
                    <th>Prix</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($orders as $row) { ?>
                <tr>
                    <td><?= $row->order_date;?></td>
                    <td><?= $row->last_name;?></td>
                    <td><img style="width:10%" class='order_img' src="../assets/img/article/<?=$row->image;?>" /></td>
                    <td><?= $row->price;?>.00 €</td>
                    <td><a href="<?php echo base_url('public/index.php/orders/show_order/'.$row->id_order);?>" class="edit"><img src="../assets/img/site/look.png" width="20%" class="material-icons" data-toggle="tooltip" title="Voir"/></a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>
   

<!-- <script src="/js/jquery.min.js"></script> -->
<!-- <script src="../public/js/bootstrap.bundle.min.js"></script> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
 
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
 
<script>
    $(document).ready(function(){
        $('#orders').DataTable();
        
        
    });
</script>
</body>
</html>