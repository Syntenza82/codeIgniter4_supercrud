<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Welcome to CodeIgniter 4!</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico" />
	<link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url(); ?>/public/assets/css/welcome-message.css" rel="stylesheet" type="text/css">
	<link href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">
	

</head>
	
<!-- HEADER: MENU + MESSAGE SESSION -->
<header>
	<?php include "nav.php";?>
</header>
	<?php include "msgSession.php";?>
<body>
		<!--CATEGORY CARDS-->
	<div id="welcome_page">
		<div class="container">
			<?php foreach($categories as $category) { ?>
				<a href="<?php echo site_url('public/index.php/product_store/'.$category['category_id']) ?>" class="product-view mb-2">
					<div class="my-card" style="background-image:url(<?=base_url();?>/public/assets/img/categories/<?= $category['category_image'] ?>)">
						<div class="my-card-body">
							<h2 style="" class="card-title"><?= $category['category_name'];?></h2>
							<!-- <p><?= $category['category_description'];?></p> -->
							<!-- <a href="<?=base_url();?>/public/index.php/products" class="product-view">Manage Products</a> -->
						</div>
					</div>
				</a>
			<?php } ?>  
		</div>		
	</div>
			
	<footer>			
		<?php include "footer.php";?>
	</footer>

</body>

</html>