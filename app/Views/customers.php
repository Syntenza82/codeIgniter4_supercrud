<?php
   if(!isset($_SESSION['email'])){
     
     redirect()->to(base_url('public/index.php/signin'));
  }
?>
<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Codeigniter 4 customers List Example</title>
  <link rel="stylesheet" href="<?php echo base_url();?>/public/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
</head>
<header>
    <?php include "nav.php";?>
  </header>
<body>
  <?php
      if(isset($_SESSION['msg'])){
          echo "<h1 class='msgSession'>" . $_SESSION['msg'] . "</h1>";
        }
  ?>
  <div class="container mt-5">
  <div class="table-title">
                  <div class="row">
                      <div class="col-sm-6 mb-5">
                          <h1>Manage<b> Customers</b></h1>
                      </div>
                      <div class="col-sm-6">
                          <p class="text-center"><a href="<?php echo site_url('public/index.php/customers/create') ?>" class="btn btn-success mb-2"><i class="material-icons"></i>Create Customer</a></p>
                      </div>
                  </div>
  </div>
    <div class="mt-3">
      <table class="table table-bordered" id="customers">
        <thead>
            <tr>
              <th>Avatar</th>
              <th>Name</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if($customers): ?>
            <?php foreach($customers as $customer): ?>
            <tr>
              <td><img width="70%" class="product-order" src="<?=base_url();?>/public/assets/img/avatar/<?= $customer['avatar'] ?>"></td>
              <td><?php echo $customer['last_name']; ?></td>
              <td><?php echo $customer['email']; ?></td>
              <td>
                <a href="<?php echo base_url('public/index.php/customers/edit/'.$customer['id']);?>"class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit"></i></a>
                <a onclick="sure()" href="<?php echo base_url('public/index.php/customers/delete/'.$customer['id']);?>"class="delete"><i class="material-icons" data-toggle="tooltip" title="Delete"></i></a>
                </td>
            </tr>
          <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
  
 
  <script>
    $(document).ready( function () {
      $('#customers').DataTable();
      function sure(){
        var r = confirm("Are you sure ?!");
        if (r) {
         alert("You pressed OK!");
        } else {
          alert("You pressed Cancel!");
          return;
        }
      } 
  } );
</script>
</body>
</html>