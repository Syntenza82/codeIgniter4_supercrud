<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ci Form Validation Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  
</head>
<body>
 
<h1 class="text-success" align="center">CI Form Validation Example</h1><br>
<div class="container">    
<h4 class="text-danger"><?php echo validation_errors(); ?></h4>
<form class="form-horizontal" method="post" action="<?php echo site_url();?>/validation/form_validate">
            <div class="form-group">
            <label class="control-label col-sm-2" for="email">Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control"  placeholder="Enter Name" name="name">
            </div>
            </div>
            
            <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control"  placeholder="Enter email" name="email">
            </div>
            </div>
            <div class="form-group">
            <label class="control-label col-sm-2" for="password">Password:</label>
            <div class="col-sm-10">          
                <input type="number" class="form-control"  placeholder="Enter Password" name="password">
            </div>
            </div>
             
            <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
                <input  type="submit" class="btn btn-success" name="submit" value="Submit">
            </div>
            </div>
  </form>
 </div>
</body>
</html>