<!DOCTYPE html>
<html>
<head>
<title>Create Customer</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
<link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
<script href="<?= base_url(); ?>/public/assets/js/create-customers.js" type="javascript/text"></script>
</head>
<body>
<header>
    <?php include "nav.php";?>   
</header>
<?php
     if(isset($_SESSION['msg'])){
        echo "<h1 style='text-align:center'>" . $_SESSION['msg'] . "</h1>";
      }
?>
<div class="container">
    <br>
    <?= \Config\Services::validation()->listErrors(); ?>
  
    <span class="d-none alert alert-success mb-3" id="res_message"></span>
    <div class="row">
    <h1>Add Customer</h1>
        <div class="col-md-12">
            <form action="<?php echo base_url('public/index.php/customers/store');?>" name="customer_create" id="customer_create" method="post" accept-charset="utf-8">
                <!-- <div class="form-group">
                    <label for="avatar">Avatar</label>
                    <input type="file" name="avatar" class="form-control" id="avatar" placeholder="Please enter Avatar name">
                </div>  -->
                <div id="alertMessage" class="alert alert-warning mb-3" style="display: none">
                <span id="alertMsg"></span>
            </div>

            <div class="d-grid text-center">
               <img class="mb-3" id="ajaxImgUpload" alt="Preview Image" src="https://via.placeholder.com/300" />
            </div>

            <div class="mb-3">
                <input type="file" name="avatar" multiple="true" id="avatar" onchange="onFileUpload(this);"
                    class="form-control form-control-lg"  accept="image/*">
            </div>
                <div class="form-group">
                    <label for="first_name">FirstName</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Please enter First name">
                </div> 
                <div class="form-group">
                    <label for="last_name">LastName</label>
                    <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Please enter Last name">
                </div> 
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="Please enter email">
                </div> 
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" name="password" class="form-control" id="password" placeholder="Please enter password">
                </div>   
                <div class="form-group">
                    <button type="submit" id="send_form" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<footer>
    <?php include "footer.php";?> 
</footer>
<script>
    /***************************************************
   @author @LudoLabTeam - Ludovic Mouly
 * @copyright  1920-2080 DEVLM - CDA 20206
 ***************************************************/

/****************************VALIDATION FORM *********************************************/

        function onFileUpload(input, id) {
            id = id || '#ajaxImgUpload';
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(id).attr('src', e.target.result).width(300)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function () {
            $('#upload_image_form').on('submit', function (e) {

                $('.uploadBtn').html('Uploading ...');
                $('.uploadBtn').prop('Disabled');

                e.preventDefault();
                if ($('#file').val() == '') {
                    alert("Choose File");
                    $('.uploadBtn').html('Upload');
                    $('.uploadBtn').prop('enabled');
                    document.getElementById("upload_image_form").reset();
                } else {
                    $.ajax({
                        url: "<?php echo base_url(); ?>/AjaxFileUpload/upload",
                        method: "POST",
                        data: new FormData(this),
                        processData: false,
                        contentType: false,
                        cache: false,
                        dataType: "json",
                        success: function (res) {
                            console.log(res.success);
                            if (res.success == true) {
                                $('#ajaxImgUpload').attr('src', 'https://via.placeholder.com/300');
                                $('#alertMsg').html(res.msg);
                                $('#alertMessage').show();
                            } else if (res.success == false) {
                                $('#alertMsg').html(res.msg);
                                $('#alertMessage').show();
                            }
                            setTimeout(function () {
                                $('#alertMsg').html('');
                                $('#alertMessage').hide();
                            }, 4000);

                            $('.uploadBtn').html('Upload');
                            $('.uploadBtn').prop('Enabled');
                            document.getElementById("upload_image_form").reset();
                        }
                    });
                }
            });
        });
    
if ($("#customer_create").length > 0) {
    
    $("#customer_create").validate({
        rules: {
            avatar: {
                required: true,
                extension: "jpg,jpeg, png"
            },
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                maxlength: 50,
                email: true,
            }, 
            password: {
                required: true,
                minlength: 6,
                password: true,
            },   
        },
        messages: {

                avatar: {
                    required: "Please enter your avatar-name",
                    extension: "You're only allowed to upload jpg or png images."
                },
                first_name: {
                    required: "Please enter your first-name",
                },
                last_name: {
                    required: "Please enter your last-name",
                },
                email: {
                    required: "Please enter your email",
                    email: "Please enter valid email",
                    maxlength: "The email name should less than or equal to 50 characters",
                },
                password: {
                    required: "Please enter your password",
                    password: "Please enter valid password",
                    minlength: "The password should more than or equal to 6 characters",
                },  
        },
    })
}
</script>
</body>
</html>