<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Welcome to CodeIgniter 4!</title>
	<meta name="description" content="The small framework with powerful features">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link href="<?= base_url(); ?>/public/assets/css/commun.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url(); ?>/public/assets/css/product-store.css" rel="stylesheet" type="text/css">
</head>
	
<!-- HEADER: MENU + HEROE SECTION -->
<header>
	<?php include "nav.php";?>
</header>
<?php
     if(isset($_SESSION['categories'])){
        echo "<h1 class='msgSession'>" . $_SESSION['msg'] . "</h1>";
      }
?>	
<body>
    <p class="text-center mt-5"><a style="padding:15px;color:#ff2000;text-decoration:none;border:2px solid #ff2000;font-size:18px;" type="button" href="<?= base_url('public/index.php/home');?>">Categories</a></p>
    <div id="product">
        <div class="card__collection clear-fix">
            <?php foreach($products as $product) { ?>
                <div class="cards cards--three" style="background-image:url(<?=base_url();?>/public/assets/img/article/<?= $product['image'] ?>)">
                    <span class="cards--three__rect-1">
                        <span class="shadow-1 text-center">
                        <span><?= $product['name'] ?></span><br>
                        <a onclick="productModal()" class="btn btn-success" data-toggle="modal"><i class="material-icons"></i> <span>Voir</span></a>
                        </span>
                    </span>
                </div>
            <?php } ?> 
        </div>
    </div>

  
    <footer>			
        <?php include "footer.php";?>
    </footer>
        <!-- SCRIPTS -->
        <script>
    
</script>


</body>

</html>